# LibreNotes

**LibreNotes** - Данный проект является логическим продолжением проекта LibreNotes (https://github.com/Nikolay-Ra/librenotes-mobile).
В рамках того проекта было реализовано мобильное приложение с синхронизацией заметок между устройствами клиента. 
В качестве продолжения проекта будет реализована поддержка новой платформы, в частности веб-браузера. Так же будет произведено улучшение имеющейся кодовой базы и расширение функционала.


Язык программирования используемые технологии: Python 3, React, Django REST framework 

Среда разработки: Visual Studio Code

**Scrum Master - Павел Хурсов** 

**Developers - Павел Хурсов, Андрей Демко, Алексей Потапов, Ефим Сироткин**

[Требования](https://gitlab.com/librenotes/frontend/-/blob/master/documents/Requirements.md)

[Графический материал](https://gitlab.com/librenotes/frontend/-/tree/master/documents%2Fmockups)

[Диаграммы]()
