from helium import *
from time import sleep

Config.implicit_wait_secs = 5


def main():
    start_chrome("127.0.0.1:3000")
    write("admin", into="Username")
    write("123", into="Password")
    click("Sign in")
    click("Add note")
    write("test", into=TextField())
    click(Button("+"))
    write("SuperTag")
    click("ADD")
    click("Add note")
    try:
        while True:
            click(Button("Delete"))
    except LookupError:
        pass

    rightclick("SuperTag")
    click("DELETE")

    kill_browser()


if __name__ == "__main__":
    main()
