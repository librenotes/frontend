import 'bootstrap/dist/js/bootstrap.bundle.min.js';
import 'bootswatch/dist/darkly/bootstrap.min.css';
import 'jquery/dist/jquery.slim.min.js';
import React from 'react';
import ReactDOM from 'react-dom';
import {HashRouter as Router} from "react-router-dom";
import App from './App';
import './index.css';

ReactDOM.render(
  <Router>
    <App />
  </Router>,
  document.getElementById("root")
);