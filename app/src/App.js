import React from 'react';
import './App.css';
import LoginPage from './LoginPage';
import RegisterPage from './RegisterPage';
import DashBoardPage from './DashBoardPage';
import EditPage from './EditPage';
import AddPage from './AddPage';
import PrivateRoute from './PrivateRoute';
import {
  Switch,
  Route
} from "react-router-dom";

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      noteId: parseInt(sessionStorage.getItem('noteId')) || null,
    };

    this.setNoteData = this.setNoteData.bind(this);
  }

  componentDidUpdate(prevProps, prevState) {
    if(prevState.noteId !== this.state.noteId) {
      sessionStorage.setItem('noteId', this.state.noteId);
    }
  }

  setNoteData(noteId) {
    this.setState({noteId: noteId});
  }

  render() {
    const noteId = this.state.noteId;

    return (
      <Switch>
        <Route path="/register">
          <RegisterPage />
        </Route>

        <PrivateRoute exact path="/dashboard/edit/:id">
          <EditPage
            noteId={noteId}
          />
        </PrivateRoute>

        <PrivateRoute exact path="/dashboard/create">
          <AddPage
          />
        </PrivateRoute>

        <PrivateRoute path="/dashboard">
          <DashBoardPage
            setNoteData={this.setNoteData}
          />
        </PrivateRoute>

        <Route path="/">
          <LoginPage />
        </Route>
      </Switch>
    );
  }
}

export default App;
