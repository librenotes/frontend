import React from 'react';
import './RegisterPage.css';
import {Link, Redirect} from "react-router-dom";
import logo from './imgs/logo.png';
import Cookies from 'js-cookie';

class RegisterPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      username: '',
      password: '',
      confirmPassword: '',
      error: '',
      redirect: ''
    };

    this.onEmailChange = this.onEmailChange.bind(this);
    this.onUsernameChange = this.onUsernameChange.bind(this);
    this.onPasswordChange = this.onPasswordChange.bind(this);
    this.onConfirmPasswordChange = this.onConfirmPasswordChange.bind(this);
    this.onRegister = this.onRegister.bind(this);
  }

  onEmailChange(event) {
    this.setState({email: event.target.value});
  }

  onUsernameChange(event) {
    this.setState({username: event.target.value});
  }

  onPasswordChange(event) {
    this.setState({password: event.target.value});
  }

  onConfirmPasswordChange(event) {
    this.setState({confirmPassword: event.target.value});
  }

  async onRegister(event) {
    event.preventDefault();

    if (!this.state.email || !this.state.username ||
        !this.state.password || !this.state.confirmPassword) {
      this.setState({error: 'All fields must be completed'});
      return;
    }

    if (this.state.password !== this.state.confirmPassword) {
      this.setState({error: "Passwords don't match"});
      return;
    }

    const form = new FormData();

    form.append('email', this.state.email);
    form.append('username', this.state.username);
    form.append('password', this.state.password);

    try {
      const response = await fetch(`/api/register/`, {
        method: 'POST',
        body: form
      });
      if (response.status === 200) {
        const responseJson = await response.json();
        Cookies.set('token', responseJson.token);
        this.setState({error: '', redirect: "/dashboard"});
      } else {
        const responseJson = await response.json();
        let errorMessage = '';
        for (let error of Object.values(responseJson)) {
          errorMessage += error[0] + ' ';
        }
        this.setState({error: errorMessage});
      }
    } catch(error) {
      this.setState({error: error.message});
    }
  }

  render() {
    const redirect = this.state.redirect;
    if (redirect) {
      return <Redirect to={redirect} />
    }

    const email = this.state.email;
    const username = this.state.username;
    const password = this.state.password;
    const passwordConfirm = this.state.confirmPassword;
    const error = this.state.error;

    return (
      <div className="screen">
        <div className="main-form">
          <div className="info-form">
            <div className="logo-block">
              <img className="logo" src={logo} alt="" />
              <div className="logo-txt">
                <p className="title mb-0">LibreNotes</p>
                <p className="txt my-0 ">Надежное хранилище для ваших заметок</p>
              </div>
            </div>
          </div>
          <div className="signup-form">
            <div className="text-center signin">
              <form className="signin-form">
                <h1 className="h3 mb-3 font-weight-normal">Sign Up</h1>
                <input value={email} type="text" className="form-control signin-form-control signin-form-first" placeholder="Email address" onChange={this.onEmailChange} />
                <input value={username} type="text" className="form-control signin-form-control signin-form-second" placeholder="Username" onChange={this.onUsernameChange} />
                <input value={password} type="password" className="form-control signin-form-control signin-form-second" placeholder="Password" onChange={this.onPasswordChange} />
                <input value={passwordConfirm} type="password" className="form-control signin-form-control signin-form-third" placeholder="Password Again" onChange={this.onConfirmPasswordChange} />
                {error ? (
                  <div className="alert alert-danger" role="alert">
                    {error}
                  </div>
                  ) : null
                }

                <button className="btn btn-lg btn-primary btn-block mb-3" type="submit" onClick={this.onRegister}>Sign Up</button>
                <Link to="/" className="link">Already have an account?</Link>
              </form>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default RegisterPage;