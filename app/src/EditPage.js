import React from 'react';
import './EditPage.css';
import TagBtn from './TagBtn.js';
import CreateTagBtn from './CreateTagBtn.js';
import { faChevronCircleLeft } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Cookies from 'js-cookie';
import {Redirect} from "react-router-dom";

class EditPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {noteText: '', noteTags: [], tags: [], error: '', redirect: ''};

    this.onClearError = this.onClearError.bind(this);
    this.onCreateTag = this.onCreateTag.bind(this);
    this.onNoteText = this.onNoteText.bind(this);
    this.onToggleChange = this.onToggleChange.bind(this);
    this.onSaveNote = this.onSaveNote.bind(this);
    this.onBack = this.onBack.bind(this);
    this.onLogOut = this.onLogOut.bind(this);
  }

  async componentDidMount() {
    const token = Cookies.get('token');
    const noteId = this.props.noteId;

    let noteText = '';
    let noteTags = [];
    try {
      const response = await fetch(`/api/notes/${noteId}/`, {
        method: 'GET',
        headers: {
          'Authorization': 'Token ' + token
        },
      });
      const responseJson = await response.json();
      if ('detail' in responseJson) {
        const key = 'detail';
        this.setState({error: responseJson[key]});
        return;
      }
        const note = responseJson;
        noteText = note.text;
        noteTags = note.tags;
    } catch(error) {
      this.setState({error: error.message});
    }

    let tags = [];
    try {
      const response = await fetch(`/api/tags/`, {
        method: 'GET',
        headers: {
          'Authorization': 'Token ' + token
        },
      });
      const responseJson = await response.json();
      if ('detail' in responseJson) {
        const key = 'detail';
        this.setState({error: responseJson[key]});
        return;
      }
        tags = responseJson;
        for (let i = 0; i < tags.length; i++) {
          tags[i].isChecked = false;
          for (let tagId of noteTags) {
            if(tags[i].id === tagId) {
              tags[i].isChecked = true;
            }
          }
        }
    } catch(error) {
      this.setState({error: error.message});
    }
    this.setState({noteText: noteText, noteTags: noteTags, tags: tags});
  }

  onClearError() {
    this.setState({error: ''});
  }

  onNoteText(event) {
    this.setState({noteText: event.target.value});
  }

  onToggleChange(tagId) {
    let tags = this.state.tags;
    for (let i = 0; i < tags.length; i++) {
      if (tags[i].id === tagId) {
        tags[i].isChecked = !tags[i].isChecked;
      }
    }
    this.setState({tags: tags});
  }

  onSaveNote() {
    const noteText = this.state.noteText;
    if (!noteText) {
      this.setState({error: 'Empty note text'});
      return;
    }

    const tags = this.state.tags;
    let selectedTagsId = [];
    for (const tag of tags) {
      if(tag.isChecked) {
        selectedTagsId.push(tag.id);
      }
    }

    const form = new FormData();

    form.append('text', noteText);
    for(const tagId of selectedTagsId) {
      form.append('tags', tagId);
    }

    const token = Cookies.get('token');
    const noteId = this.props.noteId;
    fetch(`/api/notes/${noteId}/`, {
      method: 'PUT',
      headers: {
        'Authorization': 'Token ' + token
      },
      body: form
    })
    .then((response) => response.json())
    .then((responseJson) => {
      if ('detail' in responseJson) {
        const key = 'detail';
        this.setState({error: responseJson[key]});
        return;
      }
      this.setState({redirect: '/dashboard', error: ''});
    })
    .catch((error) => {
      this.setState({error: error.message});
    });
  }

  onCreateTag(tagName) {
    if (!tagName) {
      this.setState({error: 'Empty tag name'});
      return;
    }

    const form = new FormData();

    form.append('name', tagName);

    const token = Cookies.get('token');
    fetch(`/api/tags/`, {
      method: 'POST',
      headers: {
        'Authorization': 'Token ' + token
      },
      body: form
    })
    .then((response) => response.json())
    .then((responseJson) => {
      if ('detail' in responseJson) {
        const key = 'detail';
        this.setState({error: responseJson[key]});
        return;
      }
      let tag = responseJson;
      tag.isChecked = true;

      let tags = this.state.tags;
      tags.push(tag);
      this.setState({tags: tags});
    })
    .catch((error) => {
      this.setState({error: error.message});
    });
  }

  onBack() {
    this.setState({redirect: `/dashboard`});
  }

  onLogOut() {
    Cookies.remove('token');
    this.setState({redirect: `/`});
  }

  render() {
    const redirect = this.state.redirect;
    if (redirect) {
      return <Redirect to={redirect} />
    }

    const error = this.state.error;
    const noteText = this.state.noteText;

    let tags = null;
    if(this.state.tags) {
      tags = this.state.tags.map(
      (tag) =>
        <TagBtn
          key={tag.id.toString()}
          tagId={tag.id}
          tagName={tag.name}
          isChecked={tag.isChecked}
          onToggleChange={this.onToggleChange}
        />
      );
    }

    const createTagBtn = <CreateTagBtn
                          onCreateTag={this.onCreateTag}
                        />;

    const backArrow = <FontAwesomeIcon icon={faChevronCircleLeft} size='4x' color='#a7b0a9' onClick={this.onBack} />

    return (
      <>
        {error ? (
          <div className="editor-error">
            <div className="editor-error-alert alert alert-dismissible alert-danger mb-0">
              <button type="button" className="close" data-dismiss="alert" onClick={this.onClearError}>&times;</button>
                <strong>{error}</strong>
            </div>
          </div>
        ) : null
        }
        <nav className="navbar nav-style">
          {backArrow}

          <div className="navbar-collapses mx-auto">
            <button type="button" className="btn btn-success btn-lg" onClick={this.onSaveNote}>SAVE NOTE</button>
          </div>
          <button type="button" className="btn btn-danger my-2 ml-5 my-sm-0" onClick={this.onLogOut}>Log Out</button>
        </nav>

        <div className="btn-bar">
          {tags}
          {createTagBtn}
        </div>
        <div className="input-back">
          <div className="container input-area">
            <textarea value={noteText} className="form-control form-rounded input-note" placeholder="Your Note..." onChange={this.onNoteText}></textarea>
          </div>
        </div>
      </>
    );
  }
}

export default EditPage;