import React from 'react';
import './LoginPage.css';
import logo from './imgs/logo.png'
import {Link, Redirect} from "react-router-dom";
import Cookies from 'js-cookie';

class LoginPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {username: '', password: '', error: '', redirect: ''};

    this.onUsernameChange = this.onUsernameChange.bind(this);
    this.onPasswordChange = this.onPasswordChange.bind(this);
    this.onLogin = this.onLogin.bind(this);
  }

  onUsernameChange(event) {
    this.setState({username: event.target.value});
  }

  onPasswordChange(event) {
    this.setState({password: event.target.value});
  }

  onLogin(event) {
    event.preventDefault();

    if (!this.state.username || !this.state.password) {
      this.setState({error: 'All fields must be completed'});
      return;
    }

    const form = new FormData();

    form.append('username', this.state.username);
    form.append('password', this.state.password);

    fetch('/api/token/', {
      method: 'POST',
      body: form
    })
    .then((response) => response.json())
    .then((responseJson) => {
      if (!('token' in responseJson)) {
        const key = Object.keys(responseJson);
        this.setState({error: responseJson[key]});
        return;
      }
      Cookies.set('token', responseJson.token);
      this.setState({error: '', redirect: "/dashboard"});
    })
    .catch((error) => {
      this.setState({error: error.message});
    });
  }

  render() {
    const redirect = this.state.redirect;
    if (redirect) {
      return <Redirect to={redirect} />
    }

    const username = this.state.username;
    const password = this.state.password;
    const error = this.state.error;

    return (
      <div className="screen">
        <div className="main-form">
          <div className="info-form">
            <div className="logo-block">
              <img className="logo" src={logo} alt="" />
              <div className="logo-txt">
                <p className="title mb-0">LibreNotes</p>
                <p className="txt my-0 ">Надежное хранилище для ваших заметок</p>
              </div>
            </div>
          </div>
          <div className="signup-form">
            <div className="text-center signin">
              <form className="signin-form">
                <h1 className="h3 mb-3 font-weight-normal">Sign In</h1>
                <input value={username} type="text" className="form-control signin-form-control signin-form-first" placeholder="Username" onChange={this.onUsernameChange} />
                <input value={password} type="password" className="form-control signin-form-control signin-form-second" placeholder="Password" onChange={this.onPasswordChange} />
                {error ? (
                  <div className="alert alert-danger" role="alert">
                    {error}
                  </div>
                  ) : null
                }

                <button className="btn btn-lg btn-primary btn-block mb-3" type="submit" onClick={this.onLogin}>Sign In</button>
                <Link to="/register" className="link">Don't have an account?</Link>
              </form>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default LoginPage;