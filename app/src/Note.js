import React from 'react';

class Note extends React.Component {

  render() {
    const noteId = this.props.noteId;
    const text = this.props.noteText;

    let noteTags = []
    const tagsId = this.props.noteTags;
    const allTags = this.props.allTags;
    for (const tagId of tagsId) {
      for (const tag of allTags) {
        if (tagId === tag.id) {
          noteTags.push(tag);
        }
      }
    }

    let tags = null;
    if(noteTags) {
      tags = noteTags.map(
      (tag) =>
        <span key={tag.id.toString()} className="badge badge-warning mx-1">{tag.name}</span>
      );
    }

    const created = new Date(this.props.noteCreated).toUTCString().slice(0, -7);
    return (
      <div className="card">
        <div className="card-header">
          {tags}
        </div>
        <div className="card-body">
          <p className="card-text">{text}</p>
          <div className="btn-toolbar justify-content-between" role="toolbar" aria-label="Toolbar with button groups">
            <button type="button" className="btn btn-primary" onClick={() => this.props.onOpenNote(noteId)}>Open</button>
            <button type="button" className="btn btn-danger" onClick={() => this.props.onDeleteNote(noteId)}>Delete</button>
          </div>
          <div className="text-right mt-3">
            <p className="card-text"><small className="text-muted">{created}</small></p>
          </div>
        </div>
      </div>
    );
  }
}

export default Note;