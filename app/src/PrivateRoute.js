import React from 'react';
import {
  Redirect,
  Route
} from "react-router-dom";
import Cookies from 'js-cookie';

function PrivateRoute({ children, ...rest }) {
  const token = Cookies.get('token');
  return (
    <Route
      {...rest}
      render={() =>
        token ? (
          children
        ) : (
          <Redirect to='/' />
        )
      }
    />
  );
}

export default PrivateRoute;