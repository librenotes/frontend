import React from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import { faPlus } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { createMuiTheme, ThemeProvider } from '@material-ui/core/styles';
import orange from '@material-ui/core/colors/orange';

const darkTheme = createMuiTheme({
  palette: {
    primary: orange,
    type: 'dark',
  },
});

class CreateTagBtn extends React.Component {
  constructor(props) {
    super(props);
    this.state = {open: false, tagName: ''};

    this.onOpen = this.onOpen.bind(this);
    this.onAdd = this.onAdd.bind(this);
    this.onTagChange = this.onTagChange.bind(this);
    this.onClose = this.onClose.bind(this);
  }

  onOpen() {
    this.setState({open: true, tagName: ''});
  }

  onTagChange(event) {
    this.setState({tagName: event.target.value});
  }

  onAdd() {
    const tagName = this.state.tagName.toUpperCase();
    this.props.onCreateTag(tagName);
    this.onClose();
  }

  onClose() {
    this.setState({open: false});
  }

  render() {
    const open = this.state.open;
    const tagName = this.state.tagName;
    const plus = <FontAwesomeIcon icon={faPlus} size='1x' color='#ffffff' onClick={this.onBack} />

    return (
      <>
        <div className="btn-in-bar mx-1 my-2">
          <button type="button" className="btn btn-success" onClick={this.onOpen}>
            {plus}
          </button>
        </div>
        <ThemeProvider theme={darkTheme}>
          <Dialog open={open} onClose={this.onClose} aria-labelledby="form-dialog-title">
            <DialogTitle id="form-dialog-title">New Tag</DialogTitle>
            <DialogContent>
              <DialogContentText>
                To create a new tag, enter it's name and click the ADD button:
              </DialogContentText>
              <TextField
                value={tagName}
                autoFocus
                margin="dense"
                label="Tag Name"
                type="text"
                fullWidth
                onChange={this.onTagChange}
              />
            </DialogContent>
            <DialogActions>
              <Button onClick={this.onClose} color="primary">
                Cancel
              </Button>
              <Button onClick={this.onAdd} color="primary">
                Add
              </Button>
            </DialogActions>
          </Dialog>
        </ThemeProvider>
      </>
    );
  }
}

export default CreateTagBtn;