import React from 'react';

class TagBtn extends React.Component {

  render() {
    const isChecked = this.props.isChecked;
    const tagId = this.props.tagId;
    const tagName = this.props.tagName;

    return (
      <div className="btn-group-toggle btn-in-bar" data-toggle="buttons">
          <label className={"btn btn-warning" + (!isChecked ? " active" : "")}>
            <input type="checkbox" onClick={() => this.props.onToggleChange(tagId)} /> {tagName}
          </label>
      </div>
    );
  }
}

export default TagBtn;