import React from 'react';
import './DashBoardPage.css';
import Note from './Note';
import {Redirect} from "react-router-dom";
import logo from './imgs/logo.png'
import Cookies from 'js-cookie';
import SearchTags from './SearchTags';
import AllNotesTag from './AllNotesTag';

class DashBoardPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      error: '',
      redirect: '',
      notes: [],
      tags:[],
      search: '',
      activeTagId: 0
    };

    this.setError = this.setError.bind(this);
    this.onClearError = this.onClearError.bind(this);
    this.onOpenNote = this.onOpenNote.bind(this);
    this.onDeleteNote = this.onDeleteNote.bind(this);
    this.onAddNote = this.onAddNote.bind(this);
    this.onSaveTag = this.onSaveTag.bind(this);
    this.onDeleteTag = this.onDeleteTag.bind(this);
    this.onTagClick = this.onTagClick.bind(this);
    this.onSearch = this.onSearch.bind(this);
    this.onLogOut = this.onLogOut.bind(this);
  }

  async componentDidMount() {
    const token = Cookies.get('token');
    let listNotes = [];
    try {
      const response = await fetch(`/api/notes/`, {
        method: 'GET',
        headers: {
          'Authorization': 'Token ' + token
        },
      });
      const responseJson = await response.json();
      if ('detail' in responseJson) {
        const key = 'detail';
        this.setState({error: responseJson[key]});
        return;
      }
      listNotes = responseJson;
    } catch (error) {
      this.setState({error: error.message});
    }

    let listTags = [];
    try {
      const response = await fetch(`/api/tags/`, {
        method: 'GET',
        headers: {
          'Authorization': 'Token ' + token
        },
      });
      const responseJson = await response.json();
      if ('detail' in responseJson) {
        const key = 'detail';
        this.setState({error: responseJson[key]});
        return;
      }
      listTags = responseJson;
    } catch (error) {
      this.setState({error: error.message});
    }
    this.setState({notes: listNotes, tags: listTags});
  }

  setError(error) {
    this.setState({error: error});
  }

  onClearError() {
    this.setState({error: ''});
  }

  onOpenNote(noteId) {
    this.props.setNoteData(noteId);
    this.setState({redirect: `/dashboard/edit/${noteId}`});
  }

  onAddNote() {
    this.setState({redirect: `/dashboard/create/`});
  }

  async onDeleteNote(noteId) {
    const token = Cookies.get('token');
    try {
      const response = await fetch(`/api/notes/${noteId}/`, {
        method: 'DELETE',
        headers: {
          'Authorization': 'Token ' + token
        },
      });
      if (response.status === 204) {
        let notes = this.state.notes;
        for (let i = 0; i < notes.length; i++) {
          if(notes[i].id === noteId) {
            notes.splice(i, 1);
            break;
          }
        }
        this.setState({notes: notes});
      } else {
        const responseJson = await response.json();
        if ('detail' in responseJson) {
          const key = 'detail';
          this.setState({error: responseJson[key]});
          return;
        }
      }
    } catch(error) {
      this.setState({error: error.message});
    }
  }

  onTagClick(tagId) {
    this.setState({activeTagId: tagId});
  }

  async onSaveTag(tagId, tagName) {
    if (!tagName) {
      this.setState({error: 'Empty tag name'});
      return;
    }
    const form = new FormData();

    form.append('name', tagName);

    const token = Cookies.get('token');
    try {
      const response = await fetch(`/api/tags/${tagId}/`, {
        method: 'PUT',
        headers: {
          'Authorization': 'Token ' + token
        },
        body: form
      });
      const responseJson = await response.json();
        if ('detail' in responseJson) {
          const key = 'detail';
          this.setState({error: responseJson[key]});
          return;
        }
        let tag = responseJson;
        let tags = this.state.tags;
        for(let i = 0; i < tags.length; ++i ) {
          if(tags[i].id === tag.id) {
            tags[i].name = tag.name;
            break;
          }
        }
        this.setState({tags: tags});
    } catch (error) {
      this.setState({error: error.message});
    }
  }

  async onDeleteTag(tagId) {
    const token = Cookies.get('token');
    try {
      const response = await fetch(`/api/tags/${tagId}/`, {
        method: 'DELETE',
        headers: {
          'Authorization': 'Token ' + token
        },
      });
      if (response.status === 204) {
        let tags = this.state.tags;
        for (let i = 0; i < tags.length; i++) {
          if(tags[i].id === tagId) {
            tags.splice(i, 1);
            break;
          }
        }
        if(this.state.activeTagId === tagId) {
          this.setState({tags: tags, activeTagId: 0});
        } else {
          this.setState({tags: tags});
        }
      } else {
        const responseJson = await response.json();
        if ('detail' in responseJson) {
          const key = 'detail';
          this.setState({error: responseJson[key]});
          return;
        }
      }
    } catch(error) {
      this.setState({error: error.message});
    }
  }

  onSearch(event) {
    this.setState({search: event.target.value});
  }

  onLogOut() {
    Cookies.remove('token');
    this.setState({redirect: `/`});
  }

  render() {
    const redirect = this.state.redirect;
    if (redirect) {
      return <Redirect to={redirect} />
    }

    const search = this.state.search;
    const error = this.state.error;

    const activeTagId = this.state.activeTagId;
    let searchTags = null;
    if(this.state.tags) {
      searchTags = this.state.tags.map(
      (tag) =>
        <SearchTags
          key={tag.id.toString()}
          tag={tag}
          activeTagId={activeTagId}
          onTagClick={this.onTagClick}
          onSaveTag={this.onSaveTag}
          onDeleteTag={this.onDeleteTag}
        />
      );
    }

    const allNotesTag = <AllNotesTag
                          activeTagId={activeTagId}
                          onTagClick={this.onTagClick}
    />

    let sortedNotes = [];
    const allNotes = this.state.notes;
    if(activeTagId) {
      sortedNotes = allNotes.filter(
        (note) => note.tags.includes(activeTagId)
      );
    } else {
      sortedNotes = allNotes;
    }

    let notes = null;
    const tags = this.state.tags;
    if(sortedNotes) {
      notes = sortedNotes.filter(
        (note) => note.text.includes(search)
      ).map(
      (note) =>
        <Note
          key={note.id.toString()}
          noteId={note.id}
          noteText={note.text}
          noteTags={note.tags}
          allTags={tags}
          noteCreated={note.created}
          setError={this.setError}
          onOpenNote={this.onOpenNote}
          onDeleteNote={this.onDeleteNote}
        />
      );
    }

    return (
      <>
        {error ? (
          <div className="editor-error">
            <div className="editor-error-alert alert alert-dismissible alert-danger mb-0">
              <button type="button" className="close" data-dismiss="alert" onClick={this.onClearError}>&times;</button>
                <strong>{error}</strong>
            </div>
          </div>
        ) : null
        }
        <nav className="navbar nav-style">
          <img src={logo} width="30" height="30" className="d-inline-block align-top" alt="" />
          <h5 className="mt-1">LibreNotes</h5>

          <div className="navbar-collapses mx-auto">
            <button type="button" className="btn btn-success btn-lg" onClick={this.onAddNote}>ADD NOTE</button>
          </div>
          <form className="form-inline">
            <input value={search} className="form-control search mr-sm-2" type="search" placeholder="Search" aria-label="Search" onChange={this.onSearch} />
            <button type="button" className="btn btn-danger my-2 ml-5 my-sm-0" onClick={this.onLogOut}>Log Out</button>
          </form>
        </nav>

        <div className="container-fluid content">
          <div className="row h-100">
            <div className="search-tag col-3 card bg-secondary h-100 p-0">
              <div className="card-header">YOUR TAGS</div>
              <div className="card-body overflow-auto p-0">

              <div className="card text-white bg-secondary">
                <div className="card-body list-group p-0">
                  {allNotesTag}
                  {searchTags}
                </div>
              </div>

              </div>
            </div>

            <div className="workspace border border-primary col-9 h-100 p-0">
                <div className="card-columns mx-3 my-3">
                  {notes}
              </div>
            </div>
          </div>
        </div>
      </>
    );
  }
}

export default DashBoardPage;