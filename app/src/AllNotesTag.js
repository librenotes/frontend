import React from 'react';
import {Link} from "react-router-dom";
import './AllNotesTag.css';

class AllNotesTag extends React.Component {

  render() {
    return (
      <Link to="/dashboard/0" key='0' className={"list-group-item list-group-item-action all-tag" + (this.props.activeTagId === 0 ? ' active' : '')} onClick={() => this.props.onTagClick(0)}>ALL NOTES</Link>
    );
  }
}

export default AllNotesTag;