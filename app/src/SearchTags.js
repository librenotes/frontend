import React from 'react';
import './SearchTags.css';
import {Link} from "react-router-dom";
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import { createMuiTheme, ThemeProvider } from '@material-ui/core/styles';
import orange from '@material-ui/core/colors/orange';
import red  from '@material-ui/core/colors/red';

const darkTheme = createMuiTheme({
  palette: {
    primary: orange,
    secondary: red,
    type: 'dark',
  },
});

class SearchTags extends React.Component {
  constructor(props) {
    super(props);
    this.state = {open: false, tagName: this.props.tag.name};

    this.onOpen = this.onOpen.bind(this);
    this.onTagChange = this.onTagChange.bind(this);
    this.onSave = this.onSave.bind(this);
    this.onDelete = this.onDelete.bind(this);
    this.onClose = this.onClose.bind(this);
  }

  onOpen(event) {
    event.preventDefault();
    this.setState({open: true});
  }

  onTagChange(event) {
    this.setState({tagName: event.target.value});
  }

  onSave() {
    const tagName = this.state.tagName.toUpperCase();
    const tagId = this.props.tag.id;
    this.props.onSaveTag(tagId, tagName);
    this.onClose();
  }

  onDelete() {
    const tagId = this.props.tag.id;
    this.props.onDeleteTag(tagId);
    this.onClose();
  }

  onClose() {
    this.setState({open: false});
  }

  render() {
    const tag = this.props.tag;
    const tagName = this.state.tagName;
    const open = this.state.open;

    return (
      <>
        <Link to={`/dashboard/${tag.id}`} className={"list-group-item list-group-item-action search-tag" + (this.props.activeTagId === tag.id ? ' active' : '')} onClick={() => this.props.onTagClick(tag.id)} onContextMenu={this.onOpen}>{tag.name}</Link>
        <ThemeProvider theme={darkTheme}>
        <Dialog open={open} onClose={this.onClose} aria-labelledby="form-dialog-title">
          <DialogTitle id="form-dialog-title">Edit Tag</DialogTitle>
          <DialogContent>
            <DialogContentText>
              To change name of tag, enter it's new name and click the SAVE button or to delete tag, click the DELETE button:
            </DialogContentText>
            <TextField
              value={tagName}
              autoFocus
              margin="dense"
              label="Tag Name"
              type="text"
              fullWidth
              onChange={this.onTagChange}
            />
          </DialogContent>
          <DialogActions>
            <Button onClick={this.onClose} color="primary">
              Cancel
            </Button>
            <Button  onClick={this.onSave} color="primary">
              Save
            </Button>
            <Button  onClick={this.onDelete} color="secondary">
              Delete
            </Button>
          </DialogActions>
        </Dialog>
      </ThemeProvider>
    </>
    );
  }
}

export default SearchTags;